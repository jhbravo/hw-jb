#!/home/ismart/Software/miniconda3/envs/gee/bin/python

from http.server import HTTPServer, CGIHTTPRequestHandler
import os

if __name__ == '__main__':
    try:
    
        web_dir = os.path.dirname(__file__)
        os.chdir(f"{web_dir}")
        print(f"{web_dir}")
        CGIHTTPRequestHandler.cgi_directories = ['cgi-bin/', 'htbin/']
        print(CGIHTTPRequestHandler.cgi_directories)

        httpd = HTTPServer(('', 8000),   # localhost:8000
                           CGIHTTPRequestHandler)  # CGI support.

        print("Running server. Use [ctrl]-c to terminate.")

        httpd.serve_forever()

    except KeyboardInterrupt:
        print("\nReceived keyboard interrupt. Shutting down server.")
        httpd.socket.close()